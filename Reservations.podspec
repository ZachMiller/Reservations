Pod::Spec.new do |s|

  s.name         = "Reservations"
  s.version      = "1.0.2"
  s.summary      = "Reservations allows you to select a start and end time block with a calendar view of the day."

  s.description  = <<-DESC
  Reservations uses a sophisticated table view to highlight the chosen reservation over the course of the day, with user-defined start times, end times, and blocked off slots.
                   DESC

  s.homepage     = "https://gitlab.com/ZachMiller/Reservations"

  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author       = { "Zach Miller" => "zach@200apps.co" }

  s.ios.deployment_target = "9.0"

  s.source       = { :git => "https://gitlab.com/ZachMiller/Reservations.git", :tag => "#{s.version}" }

  s.source_files  = "Reservations/*"

  s.dependency "RxSwift"
  s.dependency "RxCocoa"

end
