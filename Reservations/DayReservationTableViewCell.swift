//
//  DayReservationTableViewCell.swift
//  Reservations
//
//  Created by Zach Miller on 1/30/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class DayReservationTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    private var viewModel: DayReservationTableViewModelWrapper?
    
    // MARK: -
    @IBOutlet private var hourLabel: UILabel!
    @IBOutlet var topReservationView: ReservationView!
    @IBOutlet var bottomReservationView: ReservationView!
    
    // MARK: -
    var didTapReservationButton: ((DayReservationTableViewModel?) -> Void)?
    
    // MARK: - Public API (Methods)
    func configure(with viewModel: DayReservationTableViewModelWrapper) {
        self.viewModel = viewModel
        self.hourLabel.text = viewModel.text
        
        self.topReservationView.topLineView.backgroundColor = viewModel.topSectionStartingEdgeColor
        self.topReservationView.backgroundColor = viewModel.topSectionBackgroundColor
        
        self.bottomReservationView.topLineView.backgroundColor = viewModel.bottomSectionStartingEdgeColor
        self.bottomReservationView.backgroundColor = viewModel.bottomSectionBackgroundColor
        
        self.topReservationView.didTapReservationButton = {
            self.didTapReservationButton?(viewModel.topTableViewModel)
        }
        
        self.bottomReservationView.didTapReservationButton = {
            self.didTapReservationButton?(viewModel.bottomTableViewModel)
        }

        self.topReservationView.topLineViewHeightConstraint.constant = viewModel.topSectionStartingEdgeHeight
        self.bottomReservationView.topLineViewHeightConstraint.constant = viewModel.bottomSectionStartingEdgeHeight
        
        self.topReservationView.reservationButton.isEnabled = viewModel.topSectionButtonIsEnabled
        self.bottomReservationView.reservationButton.isEnabled = viewModel.bottomSectionButtonIsEnabled
        
        self.topReservationView.endOfTableViewLineView.isHidden = true
        self.bottomReservationView.endOfTableViewLineView.isHidden = viewModel.bottomSectionEndOfTableViewLineViewIsHidden
        
        self.topReservationView.endOfTableViewLineView.backgroundColor = UIColor.clear
        self.bottomReservationView.endOfTableViewLineView.backgroundColor = viewModel.bottomSectionEndOfTableViewEdgeColor
        
        self.bottomReservationView.endOfTableViewLineViewHeightConstraint.constant = viewModel.bottomSectionEndOfTableViewEdgeHeight
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.viewModel = nil
        self.hourLabel.text = nil
        
        self.topReservationView?.topLineView.backgroundColor = nil
        self.topReservationView?.backgroundColor = nil
        
        self.bottomReservationView?.topLineView.backgroundColor = nil
        self.bottomReservationView?.backgroundColor = nil
        
        self.topReservationView?.didTapReservationButton = nil
        self.bottomReservationView?.didTapReservationButton = nil
        
        self.topReservationView?.topLineViewHeightConstraint?.constant = 0
        self.bottomReservationView?.topLineViewHeightConstraint?.constant = 0
        
        self.topReservationView?.reservationButton.isEnabled = true
        self.bottomReservationView?.reservationButton.isEnabled = true
        
        self.topReservationView?.endOfTableViewLineView.isHidden = true
        self.bottomReservationView?.endOfTableViewLineView.isHidden = true
        
        self.topReservationView?.endOfTableViewLineView.backgroundColor = UIColor.clear
        self.bottomReservationView?.endOfTableViewLineView.backgroundColor = UIColor.clear
        
        self.bottomReservationView?.endOfTableViewLineViewHeightConstraint.constant = 0
    }
}
