//
//  StringExtension.swift
//  Reservations
//
//  Created by Zach Miller on 2/1/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import Foundation

extension String {
    
    var localize: String {
        return NSLocalizedString(self, comment: "")
    }
}
