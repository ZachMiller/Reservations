//
//  IntExtension.swift
//  Reservations
//
//  Created by Zach Miller on 2/4/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

extension Int {
    
    var isEven: Bool {
        return (self % 2 == 0)
    }
}
