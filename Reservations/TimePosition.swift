//
//  TimePosition.swift
//  Reservations
//
//  Created by Zach Miller on 28/01/2018.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

enum TimePosition {
    case am
    case pm
}

extension TimePosition: Comparable {
    static func < (lhs: TimePosition, rhs: TimePosition) -> Bool {
        switch (lhs, rhs) {
        case (.am, .pm):
            return true
        default:
            return false
        }
    }
}

extension TimePosition {
    var toString: String {
        switch self {
        case .am:
            return "AM"
        case .pm:
            return "PM"
        }
    }
}
