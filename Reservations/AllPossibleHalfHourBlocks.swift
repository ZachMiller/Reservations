//
//  AllPossibleHalfHourBlocks.swift
//  Reservations
//
//  Created by Zach Miller on 1/28/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

let allPossibleHalfHourBlocks = [
    HalfHourBlock(from: .twelve(.oClock, .am), to: .twelve(.thirty, .am)),
    HalfHourBlock(from: .twelve(.thirty, .am), to: .one(.oClock, .am)),
    
    HalfHourBlock(from: .one(.oClock, .am), to: .one(.thirty, .am)),
    HalfHourBlock(from: .one(.thirty, .am), to: .two(.oClock, .am)),
    
    HalfHourBlock(from: .two(.oClock, .am), to: .two(.thirty, .am)),
    HalfHourBlock(from: .two(.thirty, .am), to: .three(.oClock, .am)),
    
    HalfHourBlock(from: .three(.oClock, .am), to: .three(.thirty, .am)),
    HalfHourBlock(from: .three(.thirty, .am), to: .four(.oClock, .am)),
    
    HalfHourBlock(from: .four(.oClock, .am), to: .four(.thirty, .am)),
    HalfHourBlock(from: .four(.thirty, .am), to: .five(.oClock, .am)),
    
    HalfHourBlock(from: .five(.oClock, .am), to: .five(.thirty, .am)),
    HalfHourBlock(from: .five(.thirty, .am), to: .six(.oClock, .am)),
    
    HalfHourBlock(from: .six(.oClock, .am), to: .six(.thirty, .am)),
    HalfHourBlock(from: .six(.thirty, .am), to: .seven(.oClock, .am)),
    
    HalfHourBlock(from: .seven(.oClock, .am), to: .seven(.thirty, .am)),
    HalfHourBlock(from: .seven(.thirty, .am), to: .eight(.oClock, .am)),
    
    HalfHourBlock(from: .eight(.oClock, .am), to: .eight(.thirty, .am)),
    HalfHourBlock(from: .eight(.thirty, .am), to: .nine(.oClock, .am)),
    
    HalfHourBlock(from: .nine(.oClock, .am), to: .nine(.thirty, .am)),
    HalfHourBlock(from: .nine(.thirty, .am), to: .ten(.oClock, .am)),
    
    HalfHourBlock(from: .ten(.oClock, .am), to: .ten(.thirty, .am)),
    HalfHourBlock(from: .ten(.thirty, .am), to: .eleven(.oClock, .am)),
    
    HalfHourBlock(from: .eleven(.oClock, .am), to: .eleven(.thirty, .am)),
    HalfHourBlock(from: .eleven(.thirty, .am), to: .twelve(.oClock, .pm)),
    
    HalfHourBlock(from: .twelve(.oClock, .pm), to: .twelve(.thirty, .pm)),
    HalfHourBlock(from: .twelve(.thirty, .pm), to: .one(.oClock, .pm)),
    
    HalfHourBlock(from: .one(.oClock, .pm), to: .one(.thirty, .pm)),
    HalfHourBlock(from: .one(.thirty, .pm), to: .two(.oClock, .pm)),
    
    HalfHourBlock(from: .two(.oClock, .pm), to: .two(.thirty, .pm)),
    HalfHourBlock(from: .two(.thirty, .pm), to: .three(.oClock, .pm)),
    
    HalfHourBlock(from: .three(.oClock, .pm), to: .three(.thirty, .pm)),
    HalfHourBlock(from: .three(.thirty, .pm), to: .four(.oClock, .pm)),
    
    HalfHourBlock(from: .four(.oClock, .pm), to: .four(.thirty, .pm)),
    HalfHourBlock(from: .four(.thirty, .pm), to: .five(.oClock, .pm)),
    
    HalfHourBlock(from: .five(.oClock, .pm), to: .five(.thirty, .pm)),
    HalfHourBlock(from: .five(.thirty, .pm), to: .six(.oClock, .pm)),
    
    HalfHourBlock(from: .six(.oClock, .pm), to: .six(.thirty, .pm)),
    HalfHourBlock(from: .six(.thirty, .pm), to: .seven(.oClock, .pm)),
    
    HalfHourBlock(from: .seven(.oClock, .pm), to: .seven(.thirty, .pm)),
    HalfHourBlock(from: .seven(.thirty, .pm), to: .eight(.oClock, .pm)),
    
    HalfHourBlock(from: .eight(.oClock, .pm), to: .eight(.thirty, .pm)),
    HalfHourBlock(from: .eight(.thirty, .pm), to: .nine(.oClock, .pm)),
    
    HalfHourBlock(from: .nine(.oClock, .pm), to: .nine(.thirty, .pm)),
    HalfHourBlock(from: .nine(.thirty, .pm), to: .ten(.oClock, .pm)),
    
    HalfHourBlock(from: .ten(.oClock, .pm), to: .ten(.thirty, .pm)),
    HalfHourBlock(from: .ten(.thirty, .pm), to: .eleven(.oClock, .pm)),
    
    HalfHourBlock(from: .eleven(.oClock, .pm), to: .eleven(.thirty, .pm)),
    HalfHourBlock(from: .eleven(.thirty, .pm), to: .endOfDay)]
