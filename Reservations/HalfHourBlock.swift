//
//  HalfHourBlock.swift
//  Reservations
//
//  Created by Zach Miller on 1/28/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

struct HalfHourBlock {
    let startingHalfHour: HalfHour
    let endingHalfHour: HalfHour
    
    init(from startingHalfHour: HalfHour, to endingHalfHour: HalfHour) {
        self.startingHalfHour = startingHalfHour
        self.endingHalfHour = endingHalfHour
    }
}

extension HalfHourBlock: Comparable {
    static func == (lhs: HalfHourBlock, rhs: HalfHourBlock) -> Bool {
        let equalStartingTimes = (lhs.startingHalfHour == rhs.startingHalfHour)
        let equalEndingTimes = (lhs.endingHalfHour == rhs.endingHalfHour)
        
        return  equalStartingTimes && equalEndingTimes
    }
    
    static func < (lhs: HalfHourBlock, rhs: HalfHourBlock) -> Bool {
        let lhsEndingTime = lhs.endingHalfHour
        let rhsStartingTime = rhs.startingHalfHour
        
        return (lhsEndingTime <= rhsStartingTime)
    }
}

extension HalfHourBlock {
    static func get(withEndingHalfHour endingHalfHour: HalfHour) -> HalfHourBlock? {
        return allPossibleHalfHourBlocks.filter { $0.endingHalfHour == endingHalfHour }.first
    }
    
    static func get(withStartingHalfHour startingHalfHour: HalfHour) -> HalfHourBlock? {
        return allPossibleHalfHourBlocks.filter { $0.startingHalfHour == startingHalfHour }.first
    }
    
    static func get(from startingHalfHour: HalfHour, to endingHalfHour: HalfHour) -> [HalfHourBlock]? {
        return allPossibleHalfHourBlocks.filter { (($0.startingHalfHour >= startingHalfHour) && ($0.endingHalfHour <= endingHalfHour)) }.sorted()
    }    
}
