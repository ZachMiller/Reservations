//
//  DayReservationViewModelWrapper.swift
//  Reservations
//
//  Created by Zach Miller on 1/30/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import RxSwift
import RxCocoa

class DayReservationViewModelWrapper {
    
    // MARK: - Dependencies
    private let subViewModel: DayReservationViewModel
    
    // MARK: - Outputs to View Controller
    private(set) lazy var dataSource: Driver<[DayReservationTableViewModelWrapper]> = {
        self.subViewModel
            .halfHourBlocks
            .withLatestFrom(self.subViewModel.currentReservation) { (dayReservationTableViewModels: $0, reservation: $1) }
            .map { [weak self] (dayReservationTableViewModels, reservation) -> [DayReservationTableViewModelWrapper] in
                let newTableViewModelWrappers = self?.createTableViewModelWrappers(from: dayReservationTableViewModels, with: reservation) ?? []
                return newTableViewModelWrappers
            }
            .asDriver(onErrorJustReturn: [])
    }()
    
    private(set) lazy var startTimeBackgroundColor: Driver<UIColor> = {
        self.subViewModel
            .choosingState
            .map { choosingState -> UIColor in
                switch choosingState {
                case .startingTime:
                    return Constants.Colors.ChoosingStates.BackgroundSelected
                case .endingTime:
                    return Constants.Colors.ChoosingStates.BackgroundNotSelected
                }
            }
            .asDriver(onErrorJustReturn: UIColor.white)
    }()
    
    private(set) lazy var endTimeBackgroundColor: Driver<UIColor> = {
        self.subViewModel
            .choosingState
            .map { choosingState -> UIColor in
                switch choosingState {
                case .startingTime:
                    return Constants.Colors.ChoosingStates.BackgroundNotSelected
                case .endingTime:
                    return Constants.Colors.ChoosingStates.BackgroundSelected
                }
            }
            .asDriver(onErrorJustReturn: UIColor.white)
    }()
    
    private(set) lazy var startTimeTextColor: Driver<UIColor> = {
        self.subViewModel
            .choosingState
            .map { choosingState -> UIColor in
                switch choosingState {
                case .startingTime:
                    return Constants.Colors.ChoosingStates.TextSelected
                case .endingTime:
                    return Constants.Colors.ChoosingStates.TextNotSelected
                }
            }
            .asDriver(onErrorJustReturn: UIColor.white)
    }()
    
    private(set) lazy var endTimeTextColor: Driver<UIColor> = {
        self.subViewModel
            .choosingState
            .map { choosingState -> UIColor in
                switch choosingState {
                case .startingTime:
                    return Constants.Colors.ChoosingStates.TextNotSelected
                case .endingTime:
                    return Constants.Colors.ChoosingStates.TextSelected
                }
            }
            .asDriver(onErrorJustReturn: UIColor.white)
    }()
    
    private(set) lazy var instructions: Driver<String> = {
        self.subViewModel
            .choosingState
            .map { choosingState -> String in
                switch choosingState {
                case .startingTime:
                    return Constants.Texts.ChoosingStates.StartTimeInstructions
                case .endingTime:
                    return Constants.Texts.ChoosingStates.EndTimeInstructions
                }
            }
            .asDriver(onErrorJustReturn: "")
    }()
    
    private(set) lazy var reservationExists: Driver<Bool> = {
        subViewModel
            .currentReservation
            .map { reservation -> Bool in
                return (reservation.endTime != nil)
            }
            .asDriver(onErrorJustReturn: false)
    }()
    
    private(set) lazy var reservationTimes: Driver<String> = {
        subViewModel
            .currentReservation
            .filter { reservation -> Bool in
                return (reservation.endTime != nil)
            }
            .map { (reservation: Reservation) -> String in
                let start = reservation.startTime?.toUIString ?? ""
                let end = reservation.endTime?.toUIString ?? ""
                return "\(start) - \(end)"
            }
            .asDriver(onErrorJustReturn: "")
    }()
    
    private(set) lazy var timeDifference: Driver<String> = {
       subViewModel
            .currentReservation
            .filter { reservation -> Bool in
                return (reservation.endTime != nil)
            }
            .map { reservation -> String in
                guard let start = reservation.startTime,
                    let end = reservation.endTime,
                    let blocks = HalfHourBlock.get(from: start, to: end) else
                {
                    return ""
                }
                
                let amountOfHalfHours = blocks.count
                let onlyHours = amountOfHalfHours.isEven
                let amountOfHours = amountOfHalfHours / 2
                
                let hoursNumber = String(amountOfHours)
                let hoursText = (amountOfHours == 1) ? "Hour" : "Hours"
                let minutesText = onlyHours ? "" : "30 Minutes"
                
                if amountOfHours == 0 {
                    return minutesText
                } else {
                    return "\(hoursNumber) \(hoursText) \(minutesText)"
                }
            }
            .asDriver(onErrorJustReturn: "")
    }()
    
    private(set) lazy var explanationInfo: Driver<ExplanationInfo> = {
        subViewModel
            .currentReservation
            .filter { reservation -> Bool in
                return (reservation.endTime != nil)
            }
            .withLatestFrom(self.dataSource) { (reservation: $0, dataSource: $1) }
            .map { [weak self] (reservation, dataSource) -> ExplanationInfo in
                let bottomIndexPathInfo = self?.getIndexPathInfo(from: reservation, with: dataSource, for: reservation.halfHourBlocks.last)
                let explanationInfo = ExplanationInfo(bottomIndexPathInfo: bottomIndexPathInfo)
                return explanationInfo
            }
            .asDriver(onErrorJustReturn: ExplanationInfo(bottomIndexPathInfo: nil))
    }()
    
    private(set) lazy var selectAllDayIsValid: Driver<Bool> = {
       self.subViewModel
            .currentReservation
            .map { [weak self] reservation -> Bool in
                let selectAllDayIsValid = self?.verifySelectAllDayOption(with: reservation) ?? false
                return selectAllDayIsValid
            }
            .asDriver(onErrorJustReturn: false)
    }()
    
    private(set) lazy var currentReservationTimes: Observable<(startHour: Int?, startMinute: Int?, endHour: Int?, endMinute: Int?)> = {
        self.subViewModel
            .currentReservation
            .map { reservation -> (startHour: Int?, startMinute: Int?, endHour: Int?, endMinute: Int?) in
                let startTimeComponents = reservation.startTime?.toTimeComponents
                let endTimeComponents = reservation.endTime?.toTimeComponents

                return (startHour: startTimeComponents?.hour, startMinute: startTimeComponents?.minute, endHour: endTimeComponents?.hour, endMinute: endTimeComponents?.minute)
            }
            .share(replay: 1)
    }()
    
    // MARK: - Initializer
    init(dependencies: DayReservationViewModel.Dependencies, inputs: DayReservationViewModel.Inputs) {
        self.subViewModel = DayReservationViewModel(dependencies: dependencies, inputs: inputs)
    }
}

// MARK: - Internal Data Structures
extension DayReservationViewModelWrapper {
    struct ExplanationInfo {
        let bottomIndexPathInfo: IndexPathInfo?
    }
    
    struct IndexPathInfo {
        let half: Half
        let indexPath: IndexPath
    
        enum Half {
            case top
            case bottom
        }
    }
}

// MARK: - Private Methods
private extension DayReservationViewModelWrapper {
    func createTableViewModelWrappers(from dayReservationTableViewModels: [DayReservationTableViewModel], with reservation: Reservation) -> [DayReservationTableViewModelWrapper] {
        var inputTableViewModels = dayReservationTableViewModels
        var outputTableViewModelWrappers = [DayReservationTableViewModelWrapper]()
        
        if let openingTableViewModel = createOpeningTableViewModelIfNeeded(from: inputTableViewModels.first) {
            inputTableViewModels.insert(openingTableViewModel, at: inputTableViewModels.startIndex)
        }
        
        if let closingTableViewModel = createClosingTableViewModelIfNeeded(from: inputTableViewModels.last, with: reservation) {
            inputTableViewModels.insert(closingTableViewModel, at: inputTableViewModels.endIndex)
        }
        
        var topTableViewModel: DayReservationTableViewModel?
        var bottomTableViewModel: DayReservationTableViewModel?
        for tableViewModel in inputTableViewModels {
            if topTableViewModel == nil {
                topTableViewModel = tableViewModel
            } else {
                bottomTableViewModel = tableViewModel
                if let top = topTableViewModel, let bottom = bottomTableViewModel {
                    outputTableViewModelWrappers.append(DayReservationTableViewModelWrapper(topTableViewModel: top, bottomTableViewModel: bottom))
                }
                topTableViewModel = nil
            }
        }
        
        return outputTableViewModelWrappers
    }
}

// MARK: -
private extension DayReservationViewModelWrapper {
    func createOpeningTableViewModelIfNeeded(from tableViewModel: DayReservationTableViewModel?) -> DayReservationTableViewModel? {
        if let startingHalfHour = tableViewModel?.halfHourBlock.startingHalfHour,
            startingHalfHour.halfHourPosition == .oClock
        {
            if let openingHalfHourBlock = HalfHourBlock.get(withEndingHalfHour: startingHalfHour) {
                return DayReservationTableViewModel(halfHourBlock: openingHalfHourBlock, startingEdgeState: nil, filledState: nil, isPartOfAvailableHalfHourBlocks: false)
            } else {
                if let first = allPossibleHalfHourBlocks.first {
                    return DayReservationTableViewModel(halfHourBlock: first, startingEdgeState: nil, filledState: nil, isPartOfAvailableHalfHourBlocks: false)
                } else {
                    return nil
                }
            }
        } else {
            return nil
        }
    }
    
    func createClosingTableViewModelIfNeeded(from tableViewModel: DayReservationTableViewModel?, with reservation: Reservation) -> DayReservationTableViewModel? {
        if let endingHalfHour = tableViewModel?.halfHourBlock.endingHalfHour {
            if endingHalfHour == .endOfDay,
                let last = allPossibleHalfHourBlocks.last
            {
                let startingEdgeState = self.subViewModel.getClosingHalfHourBlockEdgeState(with: .endOfDay, and: reservation)
                return DayReservationTableViewModel(halfHourBlock: last, startingEdgeState: startingEdgeState, filledState: nil, isPartOfAvailableHalfHourBlocks: false, isEndOfAvailableHalfHourBlocks: true, endOfTableViewEdgeState: nil)
            } else if endingHalfHour.halfHourPosition == .oClock,
                let closingHalfHourBlock = HalfHourBlock.get(withStartingHalfHour: endingHalfHour)
            {
                let startingEdgeState = self.subViewModel.getClosingHalfHourBlockEdgeState(with: closingHalfHourBlock.startingHalfHour, and: reservation)
                let isEndOfAvailableHalfHourBlocks = (closingHalfHourBlock == self.subViewModel.availableHalfHourBlocks.last)
                let endOfTableViewEdgeState = self.subViewModel.getClosingHalfHourBlockEdgeState(with: closingHalfHourBlock.endingHalfHour, and: reservation)
                return DayReservationTableViewModel(halfHourBlock: closingHalfHourBlock, startingEdgeState: startingEdgeState, filledState: nil, isPartOfAvailableHalfHourBlocks: false, isEndOfAvailableHalfHourBlocks: isEndOfAvailableHalfHourBlocks, endOfTableViewEdgeState: endOfTableViewEdgeState)
            }
        }
        
        return nil
    }
}

// MARK: -
private extension DayReservationViewModelWrapper {
    func getIndexPathInfo(from reservation: Reservation, with dataSource: [DayReservationTableViewModelWrapper], for halfHourBlock: HalfHourBlock?) -> IndexPathInfo? {
        guard let halfHourBlock = halfHourBlock else {
            return nil
        }
        
        let _tableViewModelWrapper = dataSource.filter { tableViewModelWrapper -> Bool in
            (tableViewModelWrapper.topTableViewModel.halfHourBlock == halfHourBlock) ||
                (tableViewModelWrapper.bottomTableViewModel.halfHourBlock == halfHourBlock)
            }.first
        
        guard let tableViewModelWrapper = _tableViewModelWrapper, let row = dataSource.index(of: tableViewModelWrapper)  else {
            return nil
        }
        
        let half: IndexPathInfo.Half
        if tableViewModelWrapper.bottomTableViewModel.halfHourBlock.startingHalfHour == .twelve(.oClock, .am) ||
            tableViewModelWrapper.topTableViewModel.halfHourBlock != halfHourBlock
        {
            half = .bottom
        } else {
            half = .top
        }
        
        let indexPath = IndexPath(row: row, section: 0)
        return IndexPathInfo(half: half, indexPath: indexPath)
    }
}

// MARK: -
private extension DayReservationViewModelWrapper {
    func verifySelectAllDayOption(with reservation: Reservation) -> Bool {
        let blockedOffReservationsExist = (!self.subViewModel.blockedOffReservations.isEmpty)
        let allDayIsAlreadySelected =
            ((self.subViewModel.availableHalfHourBlocks.first?.startingHalfHour == reservation.startTime) &&
                (self.subViewModel.availableHalfHourBlocks.last?.endingHalfHour == reservation.endTime))
        
        return !(blockedOffReservationsExist || allDayIsAlreadySelected)
    }
}
