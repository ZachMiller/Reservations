//
//  DateExtension.swift
//  Reservations
//
//  Created by Zach Miller on 1/31/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import Foundation

extension Date {
    
    // MARK: - Computed Properties
    var hour: Int? {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour], from: self)
        return components.hour
    }
    
    var minute: Int? {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.minute], from: self)
        return components.minute
    }
}
