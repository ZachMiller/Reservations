//
//  ReservationView.swift
//  Reservations
//
//  Created by Zach Miller on 1/30/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class ReservationView: UIView {
    
    // MARK: - Properties
    private var contentView: UIView?

    @IBOutlet var topLineView: UIView!
    @IBOutlet var topLineViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var reservationButton: UIButton!
    @IBOutlet var endOfTableViewLineView: UIView!
    @IBOutlet var endOfTableViewLineViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: -
    var didTapReservationButton: (() -> Void)?

    // MARK: - Methods
    @IBAction func touchedReservationButton(_ sender: UIButton) {
        self.didTapReservationButton?()
    }
}

// MARK: - Xib Setup
private extension ReservationView {
    func xibSetup() {
        self.contentView = loadViewFromNib()
        self.contentView?.frame = bounds
        self.contentView?.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        if let contentView = self.contentView {
            addSubview(contentView)
        }
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view
    }
}
