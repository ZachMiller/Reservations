//
//  HalfHourPosition.swift
//  Reservations
//
//  Created by Zach Miller on 28/01/2018.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

enum HalfHourPosition {
    case oClock
    case thirty
}

extension HalfHourPosition: Comparable {
    static func < (lhs: HalfHourPosition, rhs: HalfHourPosition) -> Bool {
        switch (lhs, rhs) {
        case (.oClock, .thirty):
            return true
        default:
            return false
        }
    }
}
