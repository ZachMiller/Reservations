//
//  DayReservationViewModel.swift
//  Reservations
//
//  Created by Zach Miller on 1/29/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import RxSwift
import RxCocoa

class DayReservationViewModel {
    
    // MARK: - Dependencies
    let availableHalfHourBlocks: [HalfHourBlock]
    let blockedOffReservations: [Reservation]
    
    // MARK: - Inputs from View Model Wrapper
    private let cellTaps: Observable<HalfHourBlock>
    private let startTimeTaps: Observable<ChoosingState>
    private let endTimeTaps: Observable<ChoosingState>
    private let removeTaps: Observable<Void>
    private let selectAllDayTaps: Observable<Void>
    
    // MARK: - Private Observables
    private let previousReservationBehaviorSubject = BehaviorSubject<Reservation>(value: Reservation(startTime: nil, endTime: nil))
    private var previousReservationObservable: Observable<Reservation> { return previousReservationBehaviorSubject.asObservable() }
    
    private let switchToNewChoosingTimePublishSubject = PublishSubject<ChoosingState>()
    private var switchToNewChoosingTimeObservable: Observable<ChoosingState> { return switchToNewChoosingTimePublishSubject.asObservable() }
    
    private lazy var reservationFromCellTap: Observable<Reservation> = {
        self.cellTaps
            .withLatestFrom(self.choosingState) { (halfHourBlock: $0, choosingState: $1) }
            .map { [weak self] input -> (halfHour: HalfHour, choosingState: ChoosingState) in
                let selectedHalfHour = self?.determineHalfHour(fromSelected: input.halfHourBlock, with: input.choosingState) ?? HalfHour.twelve(.oClock, .am)
                return (halfHour: selectedHalfHour, choosingState: input.choosingState)
            }
            .withLatestFrom(self.previousReservationObservable) { (halfHour: $0.halfHour, choosingState: $0.choosingState, previousReservation: $1) }
            .filter { [weak self] input -> Bool in
                let selectionIsValid = self?.verify(selected: input.halfHour, with: input.choosingState, andPreviousReservation: input.previousReservation) ?? false
                return selectionIsValid
            }
            .do(onNext: { [weak self] input in
                if input.choosingState == .startingTime {
                    self?.switchToNewChoosingTimePublishSubject.onNext(.endingTime)
                }
            })
            .map { [weak self] input -> Reservation in
                let newReservation = self?.getReservation(fromSelected: input.halfHour, with: input.choosingState, andPreviousReservation: input.previousReservation) ?? Reservation(startTime: nil, endTime: nil)
                return newReservation
            }
            .share(replay: 1)
    }()
    
    private lazy var reservationFromRemoveTap: Observable<Reservation> = {
        self.removeTaps
            .do(onNext: { [weak self] _ in
                self?.switchToNewChoosingTimePublishSubject.onNext(.startingTime)
             })
            .map { _ -> Reservation in
                return Reservation(startTime: nil, endTime: nil)
            }
            .share(replay: 1)
    }()
    
    private lazy var reservationFromSelectAllDayTap: Observable<Reservation> = {
       self.selectAllDayTaps
        .do(onNext: { [weak self] _ in
            self?.switchToNewChoosingTimePublishSubject.onNext(.endingTime)
        })
        .map { _ -> Reservation in
            let startTime = self.availableHalfHourBlocks.first?.startingHalfHour
            let endTime = self.availableHalfHourBlocks.last?.endingHalfHour
            return Reservation(startTime: startTime, endTime: endTime)
        }
        .share(replay: 1)
    }()
    
    // MARK: - Outputs to View Model Wrapper
    lazy var currentReservation: Observable<Reservation> = {
        Observable.merge(self.reservationFromCellTap, self.reservationFromRemoveTap, self.reservationFromSelectAllDayTap)
            .do(onNext: { [weak self] reservation in
                self?.previousReservationBehaviorSubject.onNext(reservation)
            })
            .startWith(Reservation(startTime: nil, endTime: nil))
            .share(replay: 1)
    }()
    
    private(set) lazy var halfHourBlocks: Observable<[DayReservationTableViewModel]> = {
        self.currentReservation
            .map { [weak self] reservation -> [DayReservationTableViewModel] in
                let newTableViewModels = self?.getTableViewModels(from: reservation) ?? []
                return newTableViewModels
            }
            .share(replay: 1)
    }()
    
    private(set) lazy var choosingState: Observable<ChoosingState> = {
        Observable.merge(self.startTimeTaps, self.endTimeTaps, self.switchToNewChoosingTimeObservable)
            .startWith(.startingTime)
            .share(replay: 1)
    }()
    
    // MARK: - Type Aliases
    typealias Dependencies = (availableHalfHourBlocks: [HalfHourBlock], blockedOffReservations: [Reservation])
    typealias Inputs = (cellTaps: Observable<DayReservationTableViewModel>, startTimeTaps: Observable<Void>, endTimeTaps: Observable<Void>, removeTaps: Observable<Void>, selectAllDayTaps: Observable<Void>)
    
    // MARK: - Initializer
    init(dependencies: Dependencies, inputs: Inputs) {
        self.availableHalfHourBlocks = dependencies.availableHalfHourBlocks
        self.blockedOffReservations = dependencies.blockedOffReservations
        
        self.cellTaps = inputs.cellTaps.map { $0.halfHourBlock }.share(replay: 1)
        self.startTimeTaps = inputs.startTimeTaps.map { ChoosingState.startingTime }.share(replay: 1)
        self.endTimeTaps = inputs.endTimeTaps.map { ChoosingState.endingTime }.share(replay: 1)
        self.removeTaps = inputs.removeTaps.share(replay: 1)
        self.selectAllDayTaps = inputs.selectAllDayTaps.share(replay: 1)
    }
}

// MARK: - Enums
extension DayReservationViewModel {
    enum ChoosingState {
        case startingTime
        case endingTime
    }
}

// MARK: - Private Methods
private extension DayReservationViewModel {
    func determineHalfHour(fromSelected halfHourBlock: HalfHourBlock, with choosingState: ChoosingState) -> HalfHour {
        switch choosingState {
        case .startingTime:
            return halfHourBlock.startingHalfHour
        case .endingTime:
            return halfHourBlock.endingHalfHour
        }
    }
    
    func verify(selected halfHour: HalfHour, with choosingState: ChoosingState, andPreviousReservation previousReservation: Reservation) -> Bool {
        switch choosingState {
        case .startingTime:
            return verifyForStartingTime(selected: halfHour, withPreviousReservation: previousReservation)
        case .endingTime:
            return verifyForEndingTime(selected: halfHour, withPreviousReservation: previousReservation)
        }
    }
    
    func getReservation(fromSelected halfHour: HalfHour, with choosingState: ChoosingState, andPreviousReservation previousReservation: Reservation) -> Reservation {
        switch choosingState {
        case .startingTime:
            return Reservation(startTime: halfHour, endTime: previousReservation.endTime)
        case .endingTime:
            return Reservation(startTime: previousReservation.startTime, endTime: halfHour)
        }
    }
    
    func getTableViewModels(from reservation: Reservation) -> [DayReservationTableViewModel] {
        let reservationHalfHourBlocks = reservation.halfHourBlocks
        let allBlockedOffHalfHourBlocks = self.allBlockedOffHalfHourBlocks
        
        var tableViewModels = [DayReservationTableViewModel]()
        
        for halfHourBlock in self.availableHalfHourBlocks {
            if reservationHalfHourBlocks.contains(halfHourBlock) {
                let tableViewModel = createTableViewModelForReservationSection(from: halfHourBlock, and: reservation)
                tableViewModels.append(tableViewModel)
            } else {
                let isBlockedOff = (allBlockedOffHalfHourBlocks.contains(halfHourBlock))
                let tableViewModel = createTableViewModelForNonReservationSection(from: halfHourBlock, isBlockedOff: isBlockedOff, reservation: reservation)
                tableViewModels.append(tableViewModel)
            }
        }
        
        tableViewModels.sort(by: { $0.halfHourBlock < $1.halfHourBlock })
        return tableViewModels
    }
}

// MARK: -
private extension DayReservationViewModel {
    func verifyForStartingTime(selected halfHour: HalfHour, withPreviousReservation previousReservation: Reservation) -> Bool {
        for blockedOffReservation in self.blockedOffReservations {
            if halfHour.isContained(in: blockedOffReservation.halfHourBlocks, includingFirstArrayElement: true, includingLastArrayElement: false) {
                return false
            }
        }
        
        guard let previouslySelectedEndTime = previousReservation.endTime else {
            return true
        }
        
        if halfHour >= previouslySelectedEndTime {
            return false
        }
        
        for blockedOffReservation in self.blockedOffReservations {
            let noOverlap = HalfHour.blocksDoNotOverlapGivenTimeRange(blockedOffReservation.halfHourBlocks, between: halfHour, and: previouslySelectedEndTime)
            if !noOverlap {
                return false
            }
        }
        
        return true
    }
    
    func verifyForEndingTime(selected halfHour: HalfHour, withPreviousReservation previousReservation: Reservation) -> Bool {
        guard let previouslySelectedStartTime = previousReservation.startTime else {
            return false
        }
        
        for blockedOffReservation in self.blockedOffReservations {
            if halfHour.isContained(in: blockedOffReservation.halfHourBlocks, includingFirstArrayElement: false, includingLastArrayElement: true) {
                return false
            }
        }
        
        if halfHour <= previouslySelectedStartTime {
            return false
        }
        
        for blockedOffReservation in self.blockedOffReservations {
            let noOverlap = HalfHour.blocksDoNotOverlapGivenTimeRange(blockedOffReservation.halfHourBlocks, between: previouslySelectedStartTime, and: halfHour)
            if !noOverlap {
                return false
            }
        }
        
        return true
    }
}

// MARK: -
private extension DayReservationViewModel {
    var allBlockedOffHalfHourBlocks: [HalfHourBlock] {
        var blocks = [HalfHourBlock]()
        for blockedOffReservation in self.blockedOffReservations {
            blocks.append(contentsOf: blockedOffReservation.halfHourBlocks)
        }
        return blocks
    }
}

// MARK: -
private extension DayReservationViewModel {
    func createTableViewModelForReservationSection(from halfHourBlock: HalfHourBlock, and reservation: Reservation) -> DayReservationTableViewModel {
        var startingEdgeState = DayReservationTableViewModel.EdgeState.reservationMiddle
        if halfHourBlock.startingHalfHour == reservation.startTime {
            startingEdgeState = .reservationBorder
        }
        
        let filledState = DayReservationTableViewModel.FilledState.currentReservation
        let isEndOfAvailableHalfHourBlocks = (halfHourBlock == self.availableHalfHourBlocks.last)
        
        var endOfTableViewEdgeState: DayReservationTableViewModel.EdgeState? = nil
        if isEndOfAvailableHalfHourBlocks {
            endOfTableViewEdgeState = getClosingHalfHourBlockEdgeState(with: halfHourBlock.endingHalfHour, and: reservation)
        }
        
        return DayReservationTableViewModel(halfHourBlock: halfHourBlock, startingEdgeState: startingEdgeState, filledState: filledState, isEndOfAvailableHalfHourBlocks: isEndOfAvailableHalfHourBlocks, endOfTableViewEdgeState: endOfTableViewEdgeState)
    }
    
    func createTableViewModelForNonReservationSection(from halfHourBlock: HalfHourBlock, isBlockedOff: Bool, reservation: Reservation) -> DayReservationTableViewModel {
        let startingEdgeHalfHourPosition = halfHourBlock.startingHalfHour.halfHourPosition
        
        var startingEdgeState: DayReservationTableViewModel.EdgeState = (isBlockedOff) ? .blockedOff : .none(startingEdgeHalfHourPosition)
        if ((halfHourBlock.startingHalfHour == reservation.startTime) || (halfHourBlock.startingHalfHour == reservation.endTime)) {
            startingEdgeState = .reservationBorder
        }
        
        let filledState: DayReservationTableViewModel.FilledState = isBlockedOff ? .blockedOff : .none
        let isEndOfAvailableHalfHourBlocks = (halfHourBlock == self.availableHalfHourBlocks.last)

        var endOfTableViewEdgeState: DayReservationTableViewModel.EdgeState? = nil
        if isEndOfAvailableHalfHourBlocks {
            endOfTableViewEdgeState = getClosingHalfHourBlockEdgeState(with: halfHourBlock.endingHalfHour, and: reservation)
        }
        
        return DayReservationTableViewModel(halfHourBlock: halfHourBlock, startingEdgeState: startingEdgeState, filledState: filledState, isEndOfAvailableHalfHourBlocks: isEndOfAvailableHalfHourBlocks, endOfTableViewEdgeState: endOfTableViewEdgeState)
    }
}

// MARK: -
extension DayReservationViewModel {
    func getClosingHalfHourBlockEdgeState(with halfHour: HalfHour, and reservation: Reservation) -> DayReservationTableViewModel.EdgeState {
        let edgeState: DayReservationTableViewModel.EdgeState
        
        if halfHour == self.blockedOffReservations.last?.endTime {
            edgeState = .blockedOff
        } else if halfHour == reservation.endTime {
            edgeState = .reservationBorder
        } else {
            if halfHour == .endOfDay {
                edgeState = .none(.oClock)
            } else {
                let halfHourPosition = halfHour.halfHourPosition
                edgeState = .none(halfHourPosition)
            }
        }
        
        return edgeState
    }
}
