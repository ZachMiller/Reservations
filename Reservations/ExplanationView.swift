//
//  ExplanationView.swift
//  Reservations
//
//  Created by Zach Miller on 2/4/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class ExplanationView: UIView {
    
    // MARK: - Properties
    private var contentView: UIView?

    @IBOutlet var explanationStackView: UIStackView!
    @IBOutlet var reservationTimesLabel: UILabel!
    @IBOutlet var timeDifferenceLabel: UILabel!
    @IBOutlet var removeButton: UIButton!
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
}

// MARK: - Xib Setup
private extension ExplanationView {
    func xibSetup() {
        self.contentView = loadViewFromNib()
        self.contentView?.frame = bounds
        self.contentView?.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        if let contentView = self.contentView {
            addSubview(contentView)
        }
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view
    }
}
