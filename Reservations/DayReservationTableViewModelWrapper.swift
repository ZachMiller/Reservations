//
//  DayReservationTableViewModelWrapper.swift
//  Reservations
//
//  Created by Zach Miller on 1/30/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class DayReservationTableViewModelWrapper {
    
    // MARK: - Dependencies
    let topTableViewModel: DayReservationTableViewModel
    let bottomTableViewModel: DayReservationTableViewModel
    
    private var hour: Hour?
    
    // MARK: - Outputs to Table View Cell
    private(set) lazy var text: String? = {
        guard let hour = self.hour else {
            return nil
        }
        
        var growingText = String(hour.time)
        
        if growingText.hasPrefix("0") {
            growingText.remove(at: growingText.startIndex)
        }
        
        growingText.append(":00 ")
        growingText.append(hour.timePosition.toString)
        return growingText
    }()
    
    private(set) lazy var topSectionStartingEdgeColor: UIColor = {
       return topTableViewModel.startingEdgeColor
    }()
    
    private(set) lazy var topSectionBackgroundColor: UIColor = {
        return topTableViewModel.backgroundColor
    }()
    
    private(set) lazy var topSectionStartingEdgeHeight: CGFloat = {
        return topTableViewModel.startingEdgeHeight
    }()
    
    private(set) lazy var topSectionButtonIsEnabled: Bool = {
        return topTableViewModel.buttonIsEnabled
    }()
    
    private(set) lazy var bottomSectionStartingEdgeColor: UIColor = {
        return bottomTableViewModel.startingEdgeColor
    }()
    
    private(set) lazy var bottomSectionBackgroundColor: UIColor = {
        return bottomTableViewModel.backgroundColor
    }()
    
    private(set) lazy var bottomSectionStartingEdgeHeight: CGFloat = {
        return bottomTableViewModel.startingEdgeHeight
    }()
    
    private(set) lazy var bottomSectionButtonIsEnabled: Bool = {
       return bottomTableViewModel.buttonIsEnabled
    }()
    
    private(set) lazy var bottomSectionEndOfTableViewLineViewIsHidden: Bool = {
       return bottomTableViewModel.endOfTableViewLineViewIsHidden
    }()
    
    private(set) lazy var bottomSectionEndOfTableViewEdgeColor: UIColor = {
        return bottomTableViewModel.endOfTableViewEdgeColor
    }()
    
    private(set) lazy var bottomSectionEndOfTableViewEdgeHeight: CGFloat = {
        return bottomTableViewModel.endOfTableViewEdgeHeight
    }()
    
    // MARK: - Initializer
    init(topTableViewModel: DayReservationTableViewModel, bottomTableViewModel: DayReservationTableViewModel) {
        self.topTableViewModel = topTableViewModel
        self.bottomTableViewModel = bottomTableViewModel
        setHour()
    }
}

// MARK: - Equatable
extension DayReservationTableViewModelWrapper: Equatable {
    static func == (lhs: DayReservationTableViewModelWrapper, rhs: DayReservationTableViewModelWrapper) -> Bool {
        return ((lhs.topTableViewModel.halfHourBlock == rhs.topTableViewModel.halfHourBlock) &&
            (lhs.bottomTableViewModel.halfHourBlock == rhs.bottomTableViewModel.halfHourBlock))
    }
}

// MARK: - Private Methods
private extension DayReservationTableViewModelWrapper {
    func setHour() {
        if self.topTableViewModel.halfHourBlock.endingHalfHour == .endOfDay {
            self.hour = Hour(timeBetweenOneAndTwelve: 12, timePosition: .am)
        } else {
            let hourInfo = self.topTableViewModel.halfHourBlock.endingHalfHour.hourInfo
            self.hour = Hour(timeBetweenOneAndTwelve: hourInfo.hour, timePosition: hourInfo.timePosition)
        }
    }
}
