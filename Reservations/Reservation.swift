//
//  Reservation.swift
//  Reservations
//
//  Created by Zach Miller on 1/28/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import Foundation

struct Reservation {
    
    // MARK: - Properties
    let startTime: HalfHour?
    let endTime: HalfHour?
    var halfHourBlocks = [HalfHourBlock]()

    // MARK: - Initializers
    init(startTime: HalfHour?, endTime: HalfHour?) {
        self.startTime = startTime
        self.endTime = endTime
        
        setUpHalfHourBlocks()
    }
    
    init(startDate: Date, endDate: Date) {
        let startTime = HalfHour.from(date: startDate, isEndingHalfHour: false)
        let endTime = HalfHour.from(date: endDate, isEndingHalfHour: true)
        self.init(startTime: startTime, endTime: endTime)
    }
}

// MARK: - Private Methods
private extension Reservation {
    mutating func setUpHalfHourBlocks() {
        guard let startTime = self.startTime, let endTime = self.endTime else {
            return
        }
        
        self.halfHourBlocks =  HalfHourBlock.get(from: startTime, to: endTime) ?? []
    }
}
