//
//  DayReservationTableView.swift
//  Reservations
//
//  Created by Zach Miller on 2/5/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class DayReservationTableView: UITableView {
    
    // MARK: - Public API (Methods)
    func getFrameForExplanationView(with cell: UITableViewCell, for explanationInfo: DayReservationViewModelWrapper.ExplanationInfo) -> CGRect {
        let x = getXForExplanationView(with: cell)
        let width = getWidthForExplanationView(with: cell)
        let height = getHeightForExplanationView(with: cell)
        let y = getYForExplanationView(with: cell, for: explanationInfo, andExplanationViewHeight: height)
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
}

// MARK: - Private Methods
private extension DayReservationTableView {
    func getXForExplanationView(with cell: UITableViewCell) -> CGFloat {
        guard let dayReservationTableViewCell = cell as? DayReservationTableViewCell,
            let topReservationView = dayReservationTableViewCell.topReservationView else
        {
            return 0.0
        }
        
        let reservationViewOriginInReservationViewBounds = topReservationView.bounds.origin
        let reservationViewOriginInTableViewBounds = self.convert(reservationViewOriginInReservationViewBounds, from: topReservationView)
        
        return reservationViewOriginInTableViewBounds.x + 10.0
    }
    
    func getYForExplanationView(with cell: UITableViewCell, for explanationInfo: DayReservationViewModelWrapper.ExplanationInfo, andExplanationViewHeight explanationViewHeight: CGFloat) -> CGFloat {
        guard let indexPathInfo = explanationInfo.bottomIndexPathInfo else {
            return 0.0
        }
        
        let baseY: CGFloat
        switch indexPathInfo.half {
        case .top:
            baseY = cell.frame.midY
        case .bottom:
            baseY = cell.frame.maxY
        }
        
        let needToDrawBelow = (indexPathInfo.indexPath.row < 2)
        let offsetY = needToDrawBelow ? 10 : (-10 - explanationViewHeight)
        return baseY + offsetY
    }
    
    func getWidthForExplanationView(with cell: UITableViewCell) -> CGFloat {
        guard let dayReservationTableViewCell = cell as? DayReservationTableViewCell,
            let reservationViewWidth = dayReservationTableViewCell.topReservationView?.bounds.width else
        {
            return 0.0
        }
        
        return (reservationViewWidth / 2.0) - 20.0
    }
    
    func getHeightForExplanationView(with cell: UITableViewCell) -> CGFloat {
        let cellHeight = cell.bounds.height
        return cellHeight * 2.0
    }
}
