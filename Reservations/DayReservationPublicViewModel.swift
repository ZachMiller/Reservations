//
//  DayReservationPublicViewModel.swift
//  Reservations
//
//  Created by Zach Miller on 2/12/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

open class DayReservationPublicViewModel {
    
    public typealias ReservationTimes = (startHour: Int?, startMinute: Int?, endHour: Int?, endMinute: Int?)
    
    // MARK: - Properties
    let startDate: Date
    let endDate: Date
    let blockedOffReservationDates: [(startDate: Date, endDate: Date)]
    
    // MARK: -
    internal(set) open var currentReservationTimes: ReservationTimes = (startHour: nil, startMinute: nil, endHour: nil, endMinute: nil)
    
    // MARK: - Initializer
    public init(startDate: Date, endDate: Date, blockedOffReservationDates: [(startDate: Date, endDate: Date)]) {
        self.startDate = startDate
        self.endDate = endDate
        self.blockedOffReservationDates = blockedOffReservationDates
    }
}

public struct ConfigurationInfo {
    let isScrollEnabled: Bool
    
    public init(isScrollEnabled: Bool = true) {
        self.isScrollEnabled = isScrollEnabled
    }
}
