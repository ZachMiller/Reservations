//
//  DayReservationView.swift
//  Reservations
//
//  Created by Zach Miller on 2/12/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import RxSwift
import RxCocoa

open class DayReservationView: UIView {
    
    // MARK: - Properties
    open var publicViewModel: DayReservationPublicViewModel?
    
    private var viewModel: DayReservationViewModelWrapper?
    
    // MARK: -
    private var contentView: UIView?

    @IBOutlet private var headerView: UIView!
    @IBOutlet private var startTimeButton: UIButton!
    @IBOutlet private var endTimeButton: UIButton!
    @IBOutlet private var instructionsLabel: UILabel!
    
    @IBOutlet private var dayReservationTableView: DayReservationTableView!
    
    private let explanationView = ExplanationView(frame: CGRect.zero)
    private let selectAllDayView = SelectAllDayView(frame: CGRect.zero)
    
    // MARK: -
    private let cellTapsPublishSubject = PublishSubject<DayReservationTableViewModel>()
    private var cellTapsObservable: Observable<DayReservationTableViewModel> { return cellTapsPublishSubject.asObservable() }
    
    // MARK: -
    private let disposeBag = DisposeBag()
    
    // MARK: - Initializers
    public init(frame: CGRect, publicViewModel: DayReservationPublicViewModel, configurationInfo: ConfigurationInfo) {
        super.init(frame: frame)
        configureView(with: publicViewModel, and: configurationInfo)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - Private Methods
private extension DayReservationView {
    func configureView(with publicViewModel: DayReservationPublicViewModel, and configurationInfo: ConfigurationInfo) {
        xibSetup()
        doInitialUISetup()
        
        let viewModel = createViewModel(with: publicViewModel)
        self.viewModel = viewModel
        setUpBindings(with: viewModel)
        setUpPublicViewModel(with: publicViewModel, from: viewModel)
        
        setUp(configurationInfo)
    }
    
    func setUpBindings(with viewModel: DayReservationViewModelWrapper) {
        drive(with: viewModel)
        setUpUI(with: viewModel)
        setUpExplanationView(with: viewModel)
    }
    
    func setUpPublicViewModel(with publicViewModel: DayReservationPublicViewModel, from viewModel: DayReservationViewModelWrapper) {
        self.publicViewModel = publicViewModel
        
        viewModel
            .currentReservationTimes
            .do(onNext: { reservationTimes in
                publicViewModel.currentReservationTimes = reservationTimes
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func setUp(_ configurationInfo: ConfigurationInfo) {
        self.dayReservationTableView.isScrollEnabled = configurationInfo.isScrollEnabled
    }
}

// MARK: -
private extension DayReservationView {
    func drive(with viewModel: DayReservationViewModelWrapper) {
        viewModel
            .dataSource
            .drive(self.dayReservationTableView.rx.items(cellIdentifier: Constants.DayReservationTableView.CellIdentifier)) { _, dayReservationTableViewModelWrapper, cell in
                if let dayReservationTableViewCell = cell as? DayReservationTableViewCell {
                    dayReservationTableViewCell.configure(with: dayReservationTableViewModelWrapper)
                    
                    dayReservationTableViewCell.didTapReservationButton = { [weak self] dayReservationTableViewModel in
                        if let tableViewModel = dayReservationTableViewModel {
                            self?.cellTapsPublishSubject.onNext(tableViewModel)
                        }
                    }
                }
            }.disposed(by: disposeBag)
    }
    
    func setUpUI(with viewModel: DayReservationViewModelWrapper) {
        viewModel
            .startTimeBackgroundColor
            .drive(onNext: { [weak self] color in
                self?.startTimeButton.backgroundColor = color
            }).disposed(by: disposeBag)
        
        viewModel
            .endTimeBackgroundColor
            .drive(onNext: { [weak self] color in
                self?.endTimeButton.backgroundColor = color
            }).disposed(by: disposeBag)
        
        viewModel
            .startTimeTextColor
            .drive(onNext: { [weak self] color in
                self?.startTimeButton.setTitleColor(color, for: .normal)
            }).disposed(by: disposeBag)
        
        viewModel
            .endTimeTextColor
            .drive(onNext: { [weak self] color in
                self?.endTimeButton.setTitleColor(color, for: .normal)
            }).disposed(by: disposeBag)
        
        viewModel.instructions.drive(self.instructionsLabel.rx.text).disposed(by: disposeBag)
    }
    
    func setUpExplanationView(with viewModel: DayReservationViewModelWrapper) {
        viewModel
            .reservationExists
            .withLatestFrom(viewModel.explanationInfo) { (reservationExists: $0, explanationInfo: $1) }
            .drive(onNext: { [weak self] (reservationExists, explanationInfo) in
                self?.updateExplanationView(reservationExists: reservationExists, for: explanationInfo)
            }).disposed(by: disposeBag)
        
        viewModel.reservationTimes.drive(self.explanationView.reservationTimesLabel.rx.text).disposed(by: disposeBag)
        viewModel.timeDifference.drive(self.explanationView.timeDifferenceLabel.rx.text).disposed(by: disposeBag)
        
        viewModel
            .selectAllDayIsValid
            .drive(onNext: { [weak self] selectAllDayIsValid in
                self?.updateSelectAllDayView(isValid: selectAllDayIsValid)
            }).disposed(by: disposeBag)
    }
}

// MARK: - Private Methods
private extension DayReservationView {
    func createViewModel(with publicViewModel: DayReservationPublicViewModel) -> DayReservationViewModelWrapper {
        let availableHalfHourBlocks = createAvailableHalfHourBlocks(fromStartDate: publicViewModel.startDate, toEndDate: publicViewModel.endDate)
        let blockedOffReservations = createBlockedOffReservations(from: publicViewModel.blockedOffReservationDates)
        
        let startTimeTaps = self.startTimeButton.rx.tap.asObservable()
        let endTimeTaps = self.endTimeButton.rx.tap.asObservable()
        let removeTaps = self.explanationView.removeButton.rx.tap.asObservable()
        let selectAllDayTaps = self.selectAllDayView.selectAllDayButton.rx.tap.asObservable()
        
        let dependencies = (availableHalfHourBlocks: availableHalfHourBlocks, blockedOffReservations: blockedOffReservations)
        let inputs = (cellTaps: self.cellTapsObservable, startTimeTaps: startTimeTaps, endTimeTaps: endTimeTaps, removeTaps: removeTaps, selectAllDayTaps: selectAllDayTaps)
        let viewModel = DayReservationViewModelWrapper(dependencies: dependencies, inputs: inputs)
        
        return viewModel
    }
    
    func createAvailableHalfHourBlocks(fromStartDate startDate: Date, toEndDate endDate: Date) -> [HalfHourBlock] {
        guard let startingHalfHour = HalfHour.from(date: startDate, isEndingHalfHour: false),
            let endingHalfHour = HalfHour.from(date: endDate, isEndingHalfHour: true) else
        {
            return []
        }
        
        let availableHalfHourBlocks = HalfHourBlock.get(from: startingHalfHour, to: endingHalfHour) ?? []
        return availableHalfHourBlocks
    }
    
    func createBlockedOffReservations(from reservationBlocks: [(startDate: Date, endDate: Date)]) -> [Reservation] {
        let blockedOffReservations = reservationBlocks.map(Reservation.init)
        let sortedBlockedOffReservations = blockedOffReservations.sorted { (reservation1, reservation2) -> Bool in
            if reservation1.startTime == reservation2.startTime { return false }
            guard let startTime1 = reservation1.startTime else { return true }
            guard let startTime2 = reservation2.startTime else { return false }
            return startTime1 < startTime2
        }
        
        return sortedBlockedOffReservations
    }
}

// MARK: -
private extension DayReservationView {
    func updateExplanationView(reservationExists: Bool, for explanationInfo: DayReservationViewModelWrapper.ExplanationInfo) {
        if reservationExists {
            if let cell = getCell(for: explanationInfo) {
                self.explanationView.frame = self.dayReservationTableView.getFrameForExplanationView(with: cell, for: explanationInfo)
                self.dayReservationTableView.addSubview(self.explanationView)
            }
        } else {
            self.explanationView.removeFromSuperview()
        }
    }
    
    func getCell(for explanationInfo: DayReservationViewModelWrapper.ExplanationInfo) -> UITableViewCell? {
        let indexPathInfo: DayReservationViewModelWrapper.IndexPathInfo? = explanationInfo.bottomIndexPathInfo
        
        if let indexPath = indexPathInfo?.indexPath {
            let cell = self.dayReservationTableView.cellForRow(at: indexPath)
            return cell
        } else {
            return nil
        }
    }
    
    func updateSelectAllDayView(isValid: Bool) {
        if isValid {
            self.explanationView.explanationStackView.addSubview(self.selectAllDayView)
            self.explanationView.explanationStackView.addArrangedSubview(self.selectAllDayView)
        } else {
            self.explanationView.explanationStackView.removeArrangedSubview(self.selectAllDayView)
            self.selectAllDayView.removeFromSuperview()
        }
    }
}

// MARK: -
private extension DayReservationView {
    func doInitialUISetup() {
        doInitialTableViewSetup()
    }
}

// MARK: -
private extension DayReservationView {
    func doInitialTableViewSetup() {
        let tableViewCellNib = UINib(nibName: Constants.DayReservationTableView.CellNibFileName, bundle: Bundle(for: type(of: self)))
        self.dayReservationTableView.register(tableViewCellNib, forCellReuseIdentifier: Constants.DayReservationTableView.CellIdentifier)
    }
}

// MARK: -
private extension DayReservationView {
    func xibSetup() {
        self.contentView = loadViewFromNib()
        self.contentView?.frame = bounds
        self.contentView?.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        if let contentView = self.contentView {
            addSubview(contentView)
        }
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view
    }
}
