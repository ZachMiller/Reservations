//
//  Constants.swift
//  Reservations
//
//  Created by Zach Miller on 2/1/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

struct Constants {
    struct DayReservationTableView {
        static let CellNibFileName = "DayReservationTableViewCell"
        static let CellIdentifier = "DayReservationTableViewCell"
    }
    
    struct Colors {
        struct ChoosingStates {
            static let TextSelected = UIColor(red: (255 / 255), green: (255 / 255), blue: (255 / 255), alpha: 1.0)
            static let TextNotSelected = UIColor(red: (0 / 255), green: (0 / 255), blue: (0 / 255), alpha: 1.0)
            static let BackgroundSelected = UIColor(red: (101 / 255), green: (174 / 255), blue: (181 / 255), alpha: 1.0)
            static let BackgroundNotSelected = UIColor(red: (255 / 255), green: (255 / 255), blue: (255 / 255), alpha: 1.0)
        }
        
        struct TimeLines {
            static let OnTheHour = UIColor(red: (234 / 255), green: (233 / 255), blue: (230 / 255), alpha: 1.0)
            static let OnTheHalfHour = UIColor(red: (244 / 255), green: (243 / 255), blue: (240 / 255), alpha: 1.0)
        }
        
        struct ReservationStates {
            static let Border = UIColor(red: (118 / 255), green: (187 / 255), blue: (194 / 255), alpha: 1.0)
            static let Middle = UIColor(red: (118 / 255), green: (187 / 255), blue: (194 / 255), alpha: 0.3)
        }
        
        struct CellBackgroundStates {
            static let BlockedOff = UIColor(red: (234 / 255), green: (233 / 255), blue: (230 / 255), alpha: 1.0)
            static let Available = UIColor(red: (255 / 255), green: (255 / 255), blue: (255 / 255), alpha: 1.0)
        }
    }
    
    struct Texts {
        struct ChoosingStates {
            static let StartTimeInstructions = "Tap to Open Time Slot to Set Start Time".localize
            static let EndTimeInstructions = "Tap to Set End Time".localize
        }
    }
    
    struct Constraints {
        struct TimeLines {
            static let ReservationBorderHeightConstant: CGFloat = 3.0
            static let ReservationMiddleHeightConstant: CGFloat = 0.0
            static let OnTheHourHeightConstant: CGFloat = 3.0
            static let OnTheHalfHourHeightConstant: CGFloat = 1.0
            static let BlockedOffHeightConstant: CGFloat = 0.0
        }
    }
}
