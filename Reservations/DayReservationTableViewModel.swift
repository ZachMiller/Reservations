//
//  DayReservationTableViewModel.swift
//  Reservations
//
//  Created by Zach Miller on 1/29/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import UIKit

class DayReservationTableViewModel {
    
    // MARK: - Dependencies
    let halfHourBlock: HalfHourBlock
    
    private let startingEdgeState: EdgeState?
    private let endOfTableViewEdgeState: EdgeState?
    private let filledState: FilledState?
    
    private let isPartOfAvailableHalfHourBlocks: Bool
    private let isEndOfAvailableHalfHourBlocks: Bool
    
    // MARK: - Outputs to Table View Model Wrapper
    private(set) lazy var startingEdgeColor: UIColor = {
        return getColor(for: self.startingEdgeState)
    }()
    
    private(set) lazy var backgroundColor: UIColor = {
        guard let filledState = self.filledState else {
            return UIColor.white
        }
        
        switch filledState {
        case .currentReservation:
            return Constants.Colors.ReservationStates.Middle
        case .blockedOff:
            return Constants.Colors.CellBackgroundStates.BlockedOff
        case .none:
            return Constants.Colors.CellBackgroundStates.Available
        }
    }()
    
    private(set) lazy var startingEdgeHeight: CGFloat = {
        return getHeight(for: self.startingEdgeState)
    }()
    
    private(set) lazy var buttonIsEnabled: Bool = {
       return self.isPartOfAvailableHalfHourBlocks
    }()
    
    private(set) lazy var endOfTableViewLineViewIsHidden: Bool = {
        return !(self.isEndOfAvailableHalfHourBlocks && halfHourBlock.endingHalfHour.halfHourPosition == .thirty)
    }()
    
    private(set) lazy var endOfTableViewEdgeColor: UIColor = {
        return getColor(for: self.endOfTableViewEdgeState)
    }()
    
    private(set) lazy var endOfTableViewEdgeHeight: CGFloat = {
        return getHeight(for: self.endOfTableViewEdgeState)
    }()
    
    // MARK: - Initializer
    init(halfHourBlock: HalfHourBlock, startingEdgeState: EdgeState?, filledState: FilledState?, isPartOfAvailableHalfHourBlocks: Bool = true, isEndOfAvailableHalfHourBlocks: Bool = false, endOfTableViewEdgeState: EdgeState? = nil) {
        self.halfHourBlock = halfHourBlock
        self.startingEdgeState = startingEdgeState
        self.filledState = filledState
        self.isPartOfAvailableHalfHourBlocks = isPartOfAvailableHalfHourBlocks
        self.isEndOfAvailableHalfHourBlocks = isEndOfAvailableHalfHourBlocks
        self.endOfTableViewEdgeState = endOfTableViewEdgeState
    }
}

// MARK: - Public API (Enums)
extension DayReservationTableViewModel {
    enum EdgeState {
        case reservationBorder
        case reservationMiddle
        case blockedOff
        case none(HalfHourPosition)
    }
    
    enum FilledState {
        case currentReservation
        case blockedOff
        case none
    }
}

// MARK: - Private Methods
private extension DayReservationTableViewModel {
    func getColor(for edgeState: EdgeState?) -> UIColor {
        guard let edgeState = edgeState else {
            return UIColor.white
        }
        
        switch edgeState {
        case .reservationBorder:
            return Constants.Colors.ReservationStates.Border
        case .reservationMiddle:
            return Constants.Colors.ReservationStates.Middle
        case .blockedOff:
            return Constants.Colors.CellBackgroundStates.BlockedOff
        case let .none(halfHourPosition):
            switch halfHourPosition {
            case .oClock:
                return Constants.Colors.TimeLines.OnTheHour
            case .thirty:
                return Constants.Colors.TimeLines.OnTheHalfHour
            }
        }
    }
    
    func getHeight(for edgeState: EdgeState?) -> CGFloat {
        guard let edgeState = edgeState else {
            return 0
        }
        
        switch edgeState {
        case .reservationBorder:
            return Constants.Constraints.TimeLines.ReservationBorderHeightConstant
        case .reservationMiddle:
            return Constants.Constraints.TimeLines.ReservationMiddleHeightConstant
        case .blockedOff:
            return Constants.Constraints.TimeLines.BlockedOffHeightConstant
        case let .none(halfHourPosition):
            switch halfHourPosition {
            case .oClock:
                return Constants.Constraints.TimeLines.OnTheHourHeightConstant
            case .thirty:
                return Constants.Constraints.TimeLines.OnTheHalfHourHeightConstant
            }
        }
    }
}
