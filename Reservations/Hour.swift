//
//  Hour.swift
//  Reservations
//
//  Created by Zach Miller on 1/30/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

struct Hour {
    
    // MARK: - Properties
    let time: Int
    let timePosition: TimePosition
    
    // MARK: - Initializer
    init?(timeBetweenOneAndTwelve: Int, timePosition: TimePosition) {
        guard ((timeBetweenOneAndTwelve >= 1) && (timeBetweenOneAndTwelve <= 12)) else {
            return nil
        }
        
        self.time = timeBetweenOneAndTwelve
        self.timePosition = timePosition
    }
}
