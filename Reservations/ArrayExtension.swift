//
//  ArrayExtension.swift
//  Reservations
//
//  Created by Zach Miller on 2/5/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

extension Array {
    
    // MARK: - Public API (Computed Properties)
    var middleElementRoundedUp: Element? {
        let middleIndex = self.indexOfMiddleElementRoundedUp
        return (middleIndex < self.count) ? self[middleIndex] : nil
    }
}

// MARK: - Private Computed Properties
private extension Array {
    var indexOfMiddleElementRoundedUp: Int {
        return (self.count / 2)
    }
}
