//
//  HalfHour.swift
//  Reservations
//
//  Created by Zach Miller on 1/28/18.
//  Copyright © 2018 Zach Miller. All rights reserved.
//

import Foundation

enum HalfHour {
    case one(HalfHourPosition, TimePosition)
    case two(HalfHourPosition, TimePosition)
    case three(HalfHourPosition, TimePosition)
    case four(HalfHourPosition, TimePosition)
    case five(HalfHourPosition, TimePosition)
    case six(HalfHourPosition, TimePosition)
    case seven(HalfHourPosition, TimePosition)
    case eight(HalfHourPosition, TimePosition)
    case nine(HalfHourPosition, TimePosition)
    case ten(HalfHourPosition, TimePosition)
    case eleven(HalfHourPosition, TimePosition)
    case twelve(HalfHourPosition, TimePosition)
    case endOfDay
}

// MARK: - Public API
extension HalfHour {
    func isContained(in halfHourBlocks: [HalfHourBlock], includingFirstArrayElement: Bool, includingLastArrayElement: Bool) -> Bool {
        let sortedHalfHourBlocks = halfHourBlocks.sorted()

        guard let firstHalfHour = sortedHalfHourBlocks.first?.startingHalfHour,
            let lastHalfHour = sortedHalfHourBlocks.last?.endingHalfHour
            else {
                return false
        }
        
        let includesFromLeft: Bool
        let includesFromRight: Bool
        
        if includingFirstArrayElement {
            includesFromLeft = (self >= firstHalfHour)
        } else {
            includesFromLeft = (self > firstHalfHour)
        }
        
        if includingLastArrayElement {
            includesFromRight = (self <= lastHalfHour)
        } else {
            includesFromRight = (self < lastHalfHour)
        }
        
        return includesFromLeft && includesFromRight
    }

    static func blocksDoNotOverlapGivenTimeRange(_ halfHourBlocks: [HalfHourBlock], between startingHalfHour: HalfHour, and endingHalfHour: HalfHour) -> Bool {
        let sortedHalfHourBlocks = halfHourBlocks.sorted()
        
        guard let firstHalfHourFromBlocks = sortedHalfHourBlocks.first?.startingHalfHour,
            let lastHalfHourFromBlocks = sortedHalfHourBlocks.last?.endingHalfHour
            else {
                return true
        }
        
        let cleanFromAbove = (lastHalfHourFromBlocks <= startingHalfHour)
        let cleanFromBelow = (firstHalfHourFromBlocks >= endingHalfHour)
        
        return (cleanFromAbove || cleanFromBelow)
    }
    
    var halfHourPosition: HalfHourPosition {
        switch self {
        case let .one(halfHourPosition, _):
            return halfHourPosition
        case let .two(halfHourPosition, _):
            return halfHourPosition
        case let .three(halfHourPosition, _):
            return halfHourPosition
        case let .four(halfHourPosition, _):
            return halfHourPosition
        case let .five(halfHourPosition, _):
            return halfHourPosition
        case let .six(halfHourPosition, _):
            return halfHourPosition
        case let .seven(halfHourPosition, _):
            return halfHourPosition
        case let .eight(halfHourPosition, _):
            return halfHourPosition
        case let .nine(halfHourPosition, _):
            return halfHourPosition
        case let .ten(halfHourPosition, _):
            return halfHourPosition
        case let .eleven(halfHourPosition, _):
            return halfHourPosition
        case let .twelve(halfHourPosition, _):
            return halfHourPosition
        case .endOfDay:
            return .thirty
        }
    }
    
    var hourInfo: (hour: Int, timePosition: TimePosition) {
        switch self {
        case let .one(_, timePosition):
            return (1, timePosition)
        case let .two(_, timePosition):
            return (2, timePosition)
        case let .three(_, timePosition):
            return (3, timePosition)
        case let .four(_, timePosition):
            return (4, timePosition)
        case let .five(_, timePosition):
            return (5, timePosition)
        case let .six(_, timePosition):
            return (6, timePosition)
        case let .seven(_, timePosition):
            return (7, timePosition)
        case let .eight(_, timePosition):
            return (8, timePosition)
        case let .nine(_, timePosition):
            return (9, timePosition)
        case let .ten(_, timePosition):
            return (10, timePosition)
        case let .eleven(_, timePosition):
            return (11, timePosition)
        case let .twelve(_, timePosition):
            return (12, timePosition)
        case .endOfDay:
            return (11, .pm)
        }
    }
    
    static func from(date: Date, isEndingHalfHour: Bool) -> HalfHour? {
        guard let hour = date.hour, let minute = date.minute else {
            return nil
        }
        
        let halfHourPosition: HalfHourPosition = (minute < 30) ? .oClock : .thirty
        let timePosition: TimePosition = (hour < 12) ? .am : .pm

        switch hour {
        case 0, 12:
            if halfHourPosition == .oClock && timePosition == .am && isEndingHalfHour {
                return HalfHour.endOfDay
            }
            
            return HalfHour.twelve(halfHourPosition, timePosition)
        case 1, 13:
            return HalfHour.one(halfHourPosition, timePosition)
        case 2, 14:
            return HalfHour.two(halfHourPosition, timePosition)
        case 3, 15:
            return HalfHour.three(halfHourPosition, timePosition)
        case 4, 16:
            return HalfHour.four(halfHourPosition, timePosition)
        case 5, 17:
            return HalfHour.five(halfHourPosition, timePosition)
        case 6, 18:
            return HalfHour.six(halfHourPosition, timePosition)
        case 7, 19:
            return HalfHour.seven(halfHourPosition, timePosition)
        case 8, 20:
            return HalfHour.eight(halfHourPosition, timePosition)
        case 9, 21:
            return HalfHour.nine(halfHourPosition, timePosition)
        case 10, 22:
            return HalfHour.ten(halfHourPosition, timePosition)
        case 11, 23:
            return HalfHour.eleven(halfHourPosition, timePosition)
        default:
            return nil
        }
    }
    
    var toUIString: String {
        if self == .endOfDay {
            return "12:00 AM"
        }
        
        let hourInfo = self.hourInfo
        let hour = hourInfo.hour
        let halfHourPosition = self.halfHourPosition
        let timePosition = hourInfo.timePosition
        
        var growingText = String(hour)
        growingText.append(":")
        let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
        growingText.append(suffix)
        return growingText
    }
    
    var toTimeComponents: (hour: Int, minute: Int) {
        if self == .endOfDay {
            return (hour: 0, minute: 0)
        }
        
        let hour: Int
        let minute: Int
        
        switch self.halfHourPosition {
        case .oClock:
            minute = 0
        case .thirty:
            minute = 30
        }
        
        switch self {
        case let .one(_, timePosition):
            switch timePosition {
            case .am: hour = 1
            case .pm: hour = 13
            }
        case let .two(_, timePosition):
            switch timePosition {
            case .am: hour = 2
            case .pm: hour = 14
            }
        case let .three(_, timePosition):
            switch timePosition {
            case .am: hour = 3
            case .pm: hour = 15
            }
        case let .four(_, timePosition):
            switch timePosition {
            case .am: hour = 4
            case .pm: hour = 16
            }
        case let .five(_, timePosition):
            switch timePosition {
            case .am: hour = 5
            case .pm: hour = 17
            }
        case let .six(_, timePosition):
            switch timePosition {
            case .am: hour = 6
            case .pm: hour = 18
            }
        case let .seven(_, timePosition):
            switch timePosition {
            case .am: hour = 7
            case .pm: hour = 19
            }
        case let .eight(_, timePosition):
            switch timePosition {
            case .am: hour = 8
            case .pm: hour = 20
            }
        case let .nine(_, timePosition):
            switch timePosition {
            case .am: hour = 9
            case .pm: hour = 21
            }
        case let .ten(_, timePosition):
            switch timePosition {
            case .am: hour = 10
            case .pm: hour = 22
            }
        case let .eleven(_, timePosition):
            switch timePosition {
            case .am: hour = 11
            case .pm: hour = 23
            }
        case let .twelve(_, timePosition):
            switch timePosition {
            case .am: hour = 0
            case .pm: hour = 12
            }
        case .endOfDay:
            hour = 0
        }
        
        return (hour: hour, minute: minute)
    }
}

// MARK: - Comparable Implementation
extension HalfHour: Comparable {
    static func == (lhs: HalfHour, rhs: HalfHour) -> Bool {
        let equalComparisonIDs = (lhs.comparisonID == rhs.comparisonID)
        return equalComparisonIDs
    }
    
    static func < (lhs: HalfHour, rhs: HalfHour) -> Bool {
        switch (lhs, rhs) {
        case (.endOfDay, .endOfDay):
            return false
        case (.endOfDay, _):
            return false
        case (_, .endOfDay):
            return true
        default:
            break
        }
        
        guard let lhsTuple = lhs.comparisonTuple, let rhsTuple = rhs.comparisonTuple else {
            return false
        }
        
        if lhsTuple.timePosition == rhsTuple.timePosition {
            if lhsTuple.time == rhsTuple.time {
                return lhsTuple.halfHourPosition < rhsTuple.halfHourPosition
            } else {
                return lhsTuple.time < rhsTuple.time
            }
        } else {
            return lhsTuple.timePosition < rhsTuple.timePosition
        }
    }
}

// MARK: - Private Computed Properties
private extension HalfHour {
    var comparisonID: String {
        switch self {
        case let .one(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "1:\(suffix)"
        case let .two(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "2:\(suffix)"
        case let .three(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "3:\(suffix)"
        case let .four(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "4:\(suffix)"
        case let .five(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "5:\(suffix)"
        case let .six(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "6:\(suffix)"
        case let .seven(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "7:\(suffix)"
        case let .eight(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "8:\(suffix)"
        case let .nine(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "9:\(suffix)"
        case let .ten(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "10:\(suffix)"
        case let .eleven(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "11:\(suffix)"
        case let .twelve(halfHourPosition, timePosition):
            let suffix = getSuffixForComparisonID(from: halfHourPosition, and: timePosition)
            return "12:\(suffix)"
        case .endOfDay:
            return "End of Day"
        }
    }
    
    var comparisonTuple: (time: Int, halfHourPosition: HalfHourPosition, timePosition: TimePosition)? {
        switch self {
        case let .twelve(halfHourPosition, timePosition):
            return (0, halfHourPosition, timePosition)
        case let .one(halfHourPosition, timePosition):
            return (1, halfHourPosition, timePosition)
        case let .two(halfHourPosition, timePosition):
            return (2, halfHourPosition, timePosition)
        case let .three(halfHourPosition, timePosition):
            return (3, halfHourPosition, timePosition)
        case let .four(halfHourPosition, timePosition):
            return (4, halfHourPosition, timePosition)
        case let .five(halfHourPosition, timePosition):
            return (5, halfHourPosition, timePosition)
        case let .six(halfHourPosition, timePosition):
            return (6, halfHourPosition, timePosition)
        case let .seven(halfHourPosition, timePosition):
            return (7, halfHourPosition, timePosition)
        case let .eight(halfHourPosition, timePosition):
            return (8, halfHourPosition, timePosition)
        case let .nine(halfHourPosition, timePosition):
            return (9, halfHourPosition, timePosition)
        case let .ten(halfHourPosition, timePosition):
            return (10, halfHourPosition, timePosition)
        case let .eleven(halfHourPosition, timePosition):
            return (11, halfHourPosition, timePosition)
        case .endOfDay:
            return nil
        }
    }
}

// MARK: - Private Methods
private extension HalfHour {
    func getSuffixForComparisonID(from halfHourPosition: HalfHourPosition, and timePosition: TimePosition) -> String {
        switch (halfHourPosition, timePosition) {
        case (.oClock, .am):
            return "00 AM"
        case (.oClock, .pm):
            return "00 PM"
        case (.thirty, .am):
            return "30 AM"
        case (.thirty, .pm):
            return "30 PM"
        }
    }
}
